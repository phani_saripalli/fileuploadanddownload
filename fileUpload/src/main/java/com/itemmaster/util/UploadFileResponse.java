package com.itemmaster.util;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "uploadFileResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class UploadFileResponse {

    private String uploadedFilePath;
    private String errorMessage;
    private String status;
}
