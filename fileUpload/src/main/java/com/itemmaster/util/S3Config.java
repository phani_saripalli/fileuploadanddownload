package com.itemmaster.util;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class S3Config {

    private String bucketName;

    private String bucketRegion;

    private String awsAccessKeyId;

    private String awsSecretAccessKey;
}
