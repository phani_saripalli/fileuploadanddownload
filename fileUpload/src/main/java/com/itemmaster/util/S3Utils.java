package com.itemmaster.util;

import java.io.File;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;

public class S3Utils {

    private static final Logger LOGGER = LoggerFactory.getLogger(S3Utils.class);

    public static AmazonS3 createS3Client(S3Config awsS3Config) {
        BasicAWSCredentials credentials = new BasicAWSCredentials(awsS3Config.getAwsAccessKeyId(), awsS3Config.getAwsSecretAccessKey());
        AmazonS3ClientBuilder s3ClientBuilder = AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(credentials));
        s3ClientBuilder.setRegion(awsS3Config.getBucketRegion());
        return s3ClientBuilder.build();
    }

    public static void uploadFile(AmazonS3 s3Client, File file, String bucketName, String keyName) throws Exception {
        try {
            s3Client.putObject(new PutObjectRequest(bucketName, keyName, file));
            LOGGER.info("Uploaded file {} using key {}", file.getAbsoluteFile(), keyName);
        } catch (AmazonServiceException serviceException) {
            LOGGER.error("Upload rejected by Amazon S3 due to reason " + serviceException.getMessage(), serviceException);
            throw new Exception(serviceException);
        } catch (AmazonClientException clientException) {
            LOGGER.error("Error occured while communicating to Amazon S3 due to reason " + clientException.getMessage(), clientException);
            throw new Exception(clientException);
        }
    }

    public static PutObjectResult uploadFile(AmazonS3 s3Client, InputStream stream, String bucketName, String keyName) throws Exception {
        try {
            PutObjectResult result = s3Client.putObject(new PutObjectRequest(bucketName, keyName, stream, new ObjectMetadata()));
            LOGGER.info("Uploaded a file to the bucket {} with key {}", bucketName, keyName);
            return result;
        } catch (AmazonServiceException serviceException) {
            LOGGER.error("Upload rejected by Amazon S3 due to reason " + serviceException.getMessage(), serviceException);
            throw new Exception(serviceException);
        } catch (AmazonClientException clientException) {
            LOGGER.error("Error occured while communicating to Amazon S3 due to reason " + clientException.getMessage(), clientException);
            throw new Exception(clientException);
        }
    }

    public static InputStream getFileInputStream(AmazonS3 s3Client,String bucketName, String keyName) throws Exception {
        InputStream inputStream = null;
        try {
            LOGGER.info("Downloading file using the key {} from bucket {}", keyName, bucketName);
            S3Object file = s3Client.getObject(new GetObjectRequest(bucketName, keyName));
            inputStream = file.getObjectContent();
        } catch (Exception serviceException) {
            LOGGER.error("Error occured while downloading file from S3 from location" + keyName, serviceException);
        }
        return inputStream;
    }

}
