package com.example.fileUpload.service;

import java.io.File;
import java.util.UUID;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.services.s3.model.PutObjectResult;
import com.itemmaster.util.UploadFileResponse;

@Service
public class FileUploadService {

    @Value("${file.data.s3folder}")
    private String s3Folder;

    @Autowired
    private S3FileUploadService s3FileUploadService;

    public UploadFileResponse uploadFileToCloudService(String userName, MultipartFile rawDataUploadedFile) throws Exception {
        UploadFileResponse uploadFileResponse = new UploadFileResponse();
        String keyName = generateS3KeyForRawDataFileUpload(rawDataUploadedFile);
        PutObjectResult putObjectResult = s3FileUploadService.uploadFileToS3(rawDataUploadedFile, keyName);
        if (putObjectResult == null) {
            throw new Exception("Exception ocurred while creating the Raw Item Data. Failed upload file to S3.");
        } else {
            uploadFileResponse.setUploadedFilePath(keyName);
            uploadFileResponse.setStatus("SUCCESS");
        }
        return uploadFileResponse;
    }

    private String generateS3KeyForRawDataFileUpload(MultipartFile rawDataUploadedFile) {
        return s3Folder + File.separator + UUID.randomUUID() + "." + getFileExtension(rawDataUploadedFile);
    }

    private String getFileExtension(MultipartFile rawDataUploadedFile) {
        return FilenameUtils.getExtension(rawDataUploadedFile.getOriginalFilename());
    }
}
