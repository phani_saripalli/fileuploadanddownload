package com.example.fileUpload.service;

import java.io.InputStream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.amazonaws.services.s3.AmazonS3;
import com.itemmaster.util.S3Config;
import com.itemmaster.util.S3Utils;

@Component
public class S3ServiceToDownloadAFile {

    @Value("${bucket.name}")
    private String bucketName;

    @Value("${bucket.region}")
    private String bucketRegion;

    @Value("${aws.access.key.id}")
    private String awsAccessKeyId;

    @Value("${aws.secret.access.key}")
    private String awsSecretAccessKey;

    public InputStream getFileFromS3Bucket(String s3FilePath) throws Exception {
        return S3Utils.getFileInputStream(createS3Client(), bucketName, s3FilePath);
    }

    private AmazonS3 createS3Client() {
        return S3Utils.createS3Client(new S3Config(bucketName, bucketRegion, awsAccessKeyId, awsSecretAccessKey));
    }
}
