package com.example.fileUpload.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.itemmaster.util.S3Config;
import com.itemmaster.util.S3Utils;

@Service
public class S3FileUploadService {

    private static final Logger LOG = LoggerFactory.getLogger(S3FileUploadService.class);

    @Value("${bucket.name}")
    private String bucketName;

    @Value("${bucket.region}")
    private String bucketRegion;

    @Value("${aws.access.key.id}")
    private String awsAccessKeyId;

    @Value("${aws.secret.access.key}")
    private String awsSecretAccessKey;

    @Value("${s3.upload.max.retries}")
    private int maxRetries;

    private AmazonS3 createS3Client() {
        S3Config s3Config = new S3Config(bucketName, bucketRegion, awsAccessKeyId, awsSecretAccessKey);
        return S3Utils.createS3Client(s3Config);
    }

    public PutObjectResult uploadFileToS3(MultipartFile itemsDataFile, String keyName) {
        boolean uploadSuccessful = false;
        int retryCount = 0;
        PutObjectResult putObjectResult = null;
        while (!uploadSuccessful && retryCount < maxRetries) {
            try {
                putObjectResult = S3Utils.uploadFile(createS3Client(), itemsDataFile.getInputStream(), bucketName, keyName);
                uploadSuccessful = true;
            } catch (Exception exception) {
                if (++retryCount == maxRetries) {
                    LOG.error("Exception when creating client items data with the path {}", keyName, exception);
                }
            }
        }
        return putObjectResult;
    }
}
