package com.example.fileUpload.service;

import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class FileDownloadService {

    @Autowired
    private S3ServiceToDownloadAFile s3ServiceToDownloadAFile;

    public InputStream downloadFileFromCloud(String filePath) throws Exception{
        try{
            InputStream excelfileInputStreamFromS3Bucket = s3ServiceToDownloadAFile.getFileFromS3Bucket(filePath);
            return excelfileInputStreamFromS3Bucket;
        } catch(Exception exception) {
            throw new Exception("unable to download the file"+ exception.getMessage());
        }
    }
}
