package com.example.fileUpload.contoller;

import java.io.InputStream;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.fileUpload.service.FileDownloadService;
import com.example.fileUpload.service.FileUploadService;
import com.itemmaster.util.UploadFileResponse;

@RestController
public class FileUploadAndDownloadController {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileUploadAndDownloadController.class);

    @Autowired
    private FileUploadService fileUploadService;

    @Autowired
    private FileDownloadService downloadService;

    @PostMapping(value = "/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_XML_VALUE)
    public UploadFileResponse createRawItemData(@RequestHeader(value = "userName") String userName, @RequestHeader(value = "password") String password,
            @RequestPart("uploadedFile") @NotNull MultipartFile rawDataUploadedFile) throws Exception {
        try {
            if (StringUtils.isEmpty(userName) || StringUtils.isEmpty(password)) {
                throw new Exception(String.format("Api User Name Should Not Be Empty For Uploading a file"));
            }
            return fileUploadService.uploadFileToCloudService(userName, rawDataUploadedFile);
        } catch (Exception exception) {
            LOGGER.error("Exception ocurred while uploading to s3", exception.getMessage());
            throw new Exception("Exception ocurred while uploading" + exception.getMessage());
        }
    }

    @GetMapping(value = "/download")
    public ResponseEntity<Resource> download(String path) throws Exception {

        if(StringUtils.isNotEmpty(path)){
            throw new Exception("Bad request and file name should not be empty");
        }
        InputStream stream = downloadService.downloadFileFromCloud(path);
        InputStreamResource resource = new InputStreamResource(stream);

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }
}
